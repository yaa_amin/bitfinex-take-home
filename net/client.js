const {PeerRPCClient} = require('grenache-nodejs-http')

class Client {
    constructor(server_id, grapeLink, orderBook, peerRepository) {
        this.server_id = server_id
        this.rpcClient = new PeerRPCClient(grapeLink, {})
        this.link = grapeLink
        this.orderBook = orderBook
        this.peerRepository = peerRepository
    }

    start() {
        this.rpcClient.init()
    }

    place_order({type, local_time, amount, unit_price}) {
        local_time = local_time || new Date()
        let received_time = new Date(local_time)

        //TODO use global_time + local_time + received_time / 3 where global_time is looked up from the dht
        let normalized_time = new Date(Math.floor(Math.abs(received_time.getTime() + local_time.getTime()) / 2))
        let order = this.orderBook.place_order(
            this.server_id,
            local_time,
            received_time,
            normalized_time,
            type,
            amount,
            unit_price
        )
        let peers = this.peerRepository.getPeers()

        let orderJson = order.toJSON()
        let payload = {
            message_type: 'order',
            payload: orderJson,
            peer: order.server_id
            //todo: signature
        }
        for (let peer of peers) {
            this.rpcClient.request(peer.peer_id, payload, {timeout: 10000}, (err, data) => {
                if (err)
                    //TODO retry
                    return console.error("an error occurred while sending order to " + peer.peer_id)
                console.log('order sent to ' + peer.peer_id)
            })
        }
    }
}


module.exports = Client
