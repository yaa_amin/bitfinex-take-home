const {PeerRPCServer} = require('grenache-nodejs-http')

class Server {
    constructor(keyPair, server_id, peersRepository, OrderBook, config) {
        this.keyPair = keyPair
        this.peersRepository = peersRepository
        this.orderBook = OrderBook
        this.port = Number(config.port)
        this.connect_timeout = Number(config.connect_timeout) || 30000
        this.link = config.link
        this.id = server_id
        this.send_timeout = Number(config.send_timeout) || 1000
    }

    start() {
        console.log('starting server with id: ' + this.id + '*')
        this.link.start()

        const peer = new PeerRPCServer(this.link, {
            timeout: this.timeout
        })
        peer.init()

        const service = peer.transport('server')
        service.listen(this.port)

        setInterval(() => {
            this.link.announce(this.id, this.port, {})
        }, this.send_timeout)

        service.on('request', (rid, key, payload, handler) => {
            console.log(payload, key)
            //TODO authenticate the payload by checking signature?

            if (payload.message_type === 'order') {
                let order = payload.payload
                let now = new Date()
                order.local_time = new Date(order.local_time)
                if (Math.abs(now - order.local_time) > 5000) { //don't accept orders that are too far in the past
                    handler.reply(null, {msg: 'error', code: 'TIME_DRIFT_TOO_MUCH'})
                } else {
                    //TODO use global_time + local_time + received_time / 3 where global_time is looked up from the dht
                    let normalized_time = new Date(Math.floor(Math.abs(now.getTime() + new Date(order.local_time.getTime())) / 2))
                    this.orderBook.place_order(
                        order.server_id, new Date(order.local_time), now, normalized_time, order.type, order.amount, order.unit_price
                    )
                    handler.reply(null, {msg: 'success', payload: {'received_at': now}})
                }
            } else {
                handler.reply(null, {msg: 'error', code: 'ERR_UNKNOWN_MESSAGE_TYPE'})
            }

        })
    }

}

module.exports = Server
