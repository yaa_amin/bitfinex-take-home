const config = require('./config')
const {Server, Client} = require('./net')
const {OrderBook, PeerRepository} = require('./repository')
const Link = require("grenache-nodejs-link");
const OrderMatchingEngine = require('./orders').OrderMatchingEngine


const keyPair = {
    publicKey: config.PUB_KEY_PEM,
    privateKey: config.PRIV_KEY_PEM
}

const peerRepository = new PeerRepository(config.ID, config.PEERS)
const orderMatchingEngine = new OrderMatchingEngine(peerRepository)
const orderBook = new OrderBook(peerRepository, orderMatchingEngine)

const link = new Link({
    grape: config.GRAPE_SERVERS[0]
})
const server = new Server(
    keyPair, config.ID, peerRepository, orderBook, {
        link: link,
        send_timeout: 10000,
        port: config.PORT,
        connect_timeout: 30000
    }
)
const client = new Client(
    config.ID, link, orderBook, peerRepository
)
server.start()
orderBook.start()
orderMatchingEngine.start()

const httpServer = require('http').createServer((req, res) => {
    let data = '';
    req.on('data', chunk => {
        data += chunk;
    })
    req.on('end', () => {
        let order = JSON.parse(data)
        client.place_order(order)
        res.end('order placed');
    })
})
httpServer.listen(config.HTTP_PORT, () => {
    console.log('http server listing on port ' + config.HTTP_PORT)
    client.start()
})