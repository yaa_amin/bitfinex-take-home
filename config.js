const crypto = require('crypto')
exports.GRAPE_SERVERS = [
    'http://127.0.0.1:30001', 'http://127.0.0.1:40001'
]

exports.PORT = Number(process.env.PORT) || 1024 + Math.floor(Math.random() * 1000)
exports.HTTP_PORT = Number(process.env.HTTP_PORT) || 1024 + Math.floor(Math.random() * 1000)

// ideally this would be an environment variable!
let keyPair = crypto.generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
        type: "pkcs1",
        format: "pem",
    },
    privateKeyEncoding: {
        type: "pkcs1",
        format: "pem",
    }
})
exports.PUB_KEY_PEM = keyPair.publicKey
exports.PRIV_KEY_PEM = keyPair.privateKey
exports.ID = process.env.PEER_ID

// it's not clear in the documentation how to listen for new peers
// joining the network so we're going to hardcode their some names in
// an environment variable
exports.PEERS = process.env.PEERS.split(",")

