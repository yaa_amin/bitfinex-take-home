
# What I would have done differently if I had time
There are a couple of things I would have done if I had more time

1. I would rely on a global logical clock to sort the orders so that the order book can be more resistant to race conditions.
2. I would authenticate the messages from peers and validate the orders more vigrously!
3. I would document the code
4. I would have written automated tests for at least the order matching engine
5. I would have supported new peers joining the exchange
6. I would retry failed attempts to publish order books7. I would design the order matching egine and the order book to use a more node.js event loop friendly architecture rather than processing and executing the orders in a tight loop
8. I would do a lot of logging!
9. Finally, I would have used protocol buffers for the protocol instead of JSON and typescript for better type safety!


# Running the code

1. Boot up two grape servers and add their addresses to the Config.js file
2. startup as many servers as possible using the following command. For example: to start 5 nodes run the following in 5 different terminals,
```bash
   HTTP_PORT=5001 PEER_ID=peer-1 PEERS=peer-2,peer-3,peer-4,peer-5 node index.js
   HTTP_PORT=5002 PEER_ID=peer-2 PEERS=peer-1,peer-3,peer-4,peer-5 node index.js
   HTTP_PORT=5003 PEER_ID=peer-3 PEERS=peer-1,peer-2,peer-4,peer-5 node index.js
   HTTP_PORT=5004 PEER_ID=peer-4 PEERS=peer-1,peer-2,peer-3,peer-5 node index.js
   HTTP_PORT=5005 PEER_ID=peer-5 PEERS=peer-1,peer-2,peer-3,peer-4 node index.js
```
3. Connect to the http interface of any of the client and send an order in the format `{type:'bid'|'sell', amount:<float>, unit_price:<float>}`
4. In a few seconds the order execution engine of each node will will execute the order if there's a match



# The BFX challenge
Hi and congratulations to your progress with Bitfinex!

Your task is to create a simplified distributed exchange

* Each client will have its own instance of the orderbook.
* Clients submit orders to their own instance of orderbook. The order is distributed to other instances, too.
* If a client's order matches with another order, any remainer is added to the orderbook, too.

Requirement:
* Use Grenache for communication between nodes
* Simple order matching engine
* You don't need to create a UI or HTTP API

You should not spend more time than 6-8 hours on the task. We know that its probably not possible to complete the task 100% in the given time.


If you don't get to the end, just write up what is missing for a complete implementation of the task. Also, if your implementation has limitation and issues, that's no big deal. Just write everything down and indicate how you could solve them, given there was more time.

Good luck!

## Tips

 - you don't need to store state in a DB or filesystem
 - it is possible to solve the task with the node std lib, async and grenache libraries
 - beware of race conditions!
 - no need for express or any other http api layers

### Setting up the DHT

```
npm i -g grenache-grape
```

```
# boot two grape servers

grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'
grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'
```

### Setting up Grenache in your project

```
npm install --save grenache-nodejs-http
npm install --save grenache-nodejs-link
```


### Example RPC server / client with "Hello World"

```js
// This RPC server will announce itself as `rpc_test`
// in our Grape Bittorrent network
// When it receives requests, it will answer with 'world'

'use strict'

const { PeerRPCServer }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')


const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCServer(link, {
  timeout: 300000
})
peer.init()

const port = 1024 + Math.floor(Math.random() * 1000)
const service = peer.transport('server')
service.listen(port)

setInterval(function () {
  link.announce('rpc_test', service.port, {})
}, 1000)

service.on('request', (rid, key, payload, handler) => {
  console.log(payload) //  { msg: 'hello' }
  handler.reply(null, { msg: 'world' })
})

```

```js
// This client will as the DHT for a service called `rpc_test`
// and then establishes a P2P connection it.
// It will then send { msg: 'hello' } to the RPC server

'use strict'

const { PeerRPCClient }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCClient(link, {})
peer.init()

peer.request('rpc_test', { msg: 'hello' }, { timeout: 10000 }, (err, data) => {
  if (err) {
    console.error(err)
    process.exit(-1)
  }
  console.log(data) // { msg: 'world' }
})
```

### More Help

 - http://blog.bitfinex.com/tutorial/bitfinex-loves-microservices-grenache/
 - https://github.com/bitfinexcom/grenache-nodejs-example-fib-client
 - https://github.com/bitfinexcom/grenache-nodejs-example-fib-server
