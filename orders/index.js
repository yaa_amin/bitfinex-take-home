const assert = require("assert");
const {Trade} = require('../repository/models')

class OrderMatchingEngine {
    /**
     * A basic matching engine that executes orders based using a FIFO algorithm
     *
     * Orders are sorted according to their price, timestamp and peer_id.
     * For bid orders, orders with higher prices are prioritized and the opposite is true for sell orders
     * @param peerRepository
     */
    constructor(peerRepository) {
        this.peerRepository = peerRepository
        this.bids = [] // idealy we could use a priority queue that sorts entries as they get added
        this.sells = [] // idealy we could use a priority queue that sorts entries as they get added
    }

    start() {
        setInterval(() => {
            // In order not to block the event loop, this should ideally be
            // in chunks but we're going with a tight loop, because of limited time
            if (this.sells.length > 0 && this.bids.length > 0) {
                for (let sell_order of this.sells.filter(sell_order => !sell_order.filled())) {
                    let seller = this.peerRepository.findPeer(sell_order.server_id)
                    let match = this.bids.find(bid_order => bid_order.unit_price >= sell_order.unit_price && !bid_order.filled())
                    while (match) {
                        let buyer = this.peerRepository.findPeer(match.server_id)
                        let trade = new Trade()
                        trade.seller = seller.peer_id
                        trade.buyer = buyer.peer_id
                        if (sell_order.remaining < match.amount) {
                            trade.amount = sell_order.remaining
                            sell_order.remaining = 0
                            match.remaining -= sell_order.remaining
                        } else if (sell_order.remaining > match.amount) {
                            trade.amount = sell_order.amount.amount
                            sell_order.remaining -= match.amount
                            match.remaining = 0
                        } else {
                            trade.amount = sell_order.amount
                            match.remaining = 0
                            sell_order.remaining = 0
                        }
                        trade.time = new Date()
                        trade.unit_price = sell_order.unit_price
                        seller.sells.push(trade)
                        buyer.bids.push(trade)
                        console.log("trade complete: " + JSON.stringify(trade))
                        match = this.bids.find(bid_order => bid_order.unit_price >= sell_order.unit_price && !bid_order.filled())
                    }
                }
                console.log("executing orders")
            }
        }, 5000)
    }

    process_order(order) {
        if (order.type === 'bid') {
            if (this._validate_bid_order(order)) {
                this.bids.push(order)
                this.bids.sort((first, second) => {
                    if (first.unit_price > second.unit_price) {
                        return -1
                    }
                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time) {
                        return -1
                    }

                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time && first.server_id > second.server_id) {
                        return -1
                    }

                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time && first.server_id === second.server_id) {
                        return 0
                    }
                    return 1

                })
                return true
            }
            return false
        } else if (order.type === 'sell') {
            if (this._validate_sell_order(order)) {
                this.sells.push(order)
                this.sells.sort((first, second) => {
                    if (first.unit_price > second.unit_price) {
                        return 1
                    }
                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time) {
                        return 1
                    }

                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time && first.server_id > second.server_id) {
                        return 1
                    }

                    if (first.unit_price === second.unit_price && first.normalized_time > second.normalized_time && first.server_id === second.server_id) {
                        return 0
                    }
                    return -1

                })
                return true
            }
            return false
        }
        throw new Error('unsupported order type')
    }

    _validate_bid_order(order) {
        // Can the peer place this order according to our view of the orderbook?
        let peer = this.peerRepository.findPeer(order.server_id)
        assert(peer !== null)
        return peer.bid_balance() > order.amount * order.unit_price;
    }

    _validate_sell_order(order) {
        // Can the peer place this order according to our view of the orderbook?
        let peer = this.peerRepository.findPeer(order.server_id)
        assert(peer !== null)
        return peer.sell_balance() > order.amount
    }
}


module.exports = {
    OrderMatchingEngine: OrderMatchingEngine
}