class Peer {
    constructor(peer_id) {
        this.peer_id = peer_id
        this.inital_buy_balance = 100000
        this.initial_sell_balance = 100000
        this.last_local_time = null
        this.bids = []
        this.sells = []
    }

    bid_balance() {
        let bids = this.bids.filter(trade => trade.type === 'bid').map(trade => trade.amount * trade.unit_price)
        let sells = this.sells.filter(trade => trade.type === 'sell').map(trade => trade.amount * trade.unit_price)
        let total_buys = bids.reduce((accum, bid) => accum + bid, 0)
        let total_sells = sells.reduce((accum, bid) => accum + bid, 0)
        return this.inital_buy_balance + total_sells - total_buys
    }

    sell_balance() {
        let bids = this.bids.map(trade => trade.amount)
        let sells = this.sells.map(trade => trade.amount)
        let total_buys = bids.reduce((accum, bid) => accum + bid, 0)
        let total_sells = sells.reduce((accum, bid) => accum + bid, 0)
        return this.initial_sell_balance + total_buys - total_sells
    }
}

class Order {
    constructor(server_id, local_time, received_timestamp, normalized_time, type, amount, unit_price) {
        this.server_id = server_id
        this.local_time = local_time
        this.received_timestamp = received_timestamp
        this.normalized_time = normalized_time
        this.type = type
        this.amount = amount
        this.unit_price = unit_price
        this.remaining = this.amount
    }

    filled() {
        return this.remaining === 0
    }

    toJSON() {
        return {
            server_id: this.server_id,
            local_time: this.local_time,
            received_timestamp: this.received_timestamp,
            type: this.type,
            amount: this.amount,
            unit_price: this.unit_price
        }
    }
}

class Trade {
    constructor(seller, buyer, amount, unit_price, time) {
        this.seller = seller
        this.buyer = buyer
        this.amount = amount
        this.unit_price = unit_price
        this.time = time
    }
}

module.exports = {
    Peer: Peer,
    Order: Order,
    Trade: Trade
}