const {Order, Peer} = require('./models')

class PeerRepository {
    constructor(server_id, initial_peer_ids) {
        this.peers = []
        this.current_server = new Peer(server_id)
        for (let peer of initial_peer_ids) {
            this.peers.push(new Peer(peer))
        }
    }

    findPeer(peer_id) {
        if(peer_id === this.current_server.peer_id){
            return this.current_server
        }
        return this.peers.find(peer => peer.peer_id === peer_id)
    }

    getPeers() {
        return this.peers
    }
}

class OrderBook {
    constructor(peerRepository, orderMatchingEngine) {
        this.peerRepository = peerRepository
        this.orderMatchingEngine = orderMatchingEngine
        this.pendingOrders = []
    }

    place_order(server_id, local_time, received_timestamp, normalized_time, type, amount, unit_price) {
        //TODO validate order
        let order = new Order(server_id, local_time, received_timestamp, normalized_time, type, amount, unit_price)
        this.pendingOrders.push(order)
        return order
    }

    start() {
        setInterval(() => {
            if (this.pendingOrders.length > 0) {
                // console.log('processing orders')
                // In order not to block the event loop, this should idealy be
                // in chunks but we're going with a tight loop, because of limited time

                // only take out orders that have been here for more than 5 seconds
                let mature_orders = []
                let un_mature_orders = []
                let start = new Date()
                this.pendingOrders.forEach(order => {
                    if (order.received_timestamp > start - 5000) {
                        mature_orders.push(order)
                    } else {
                        un_mature_orders.push(order)
                    }
                })
                this.pendingOrders = un_mature_orders
                for (let order of mature_orders) {
                    console.log('sending order to matching engine')
                    if (!this.orderMatchingEngine.process_order(order)) {
                        console.log('failed to proccess order')
                        //TODO let the order book implement an event emitter so that we can report this error to the listeners?
                    }
                }
            }
        }, 1000)
    }
}

exports.PeerRepository = PeerRepository
exports.OrderBook = OrderBook